SPSS Work

Topic3 + Topic4 - Section One of Workbook



Summary Statistics

When the distribution is skewed we get more accurate information from the Median. This is because in a skewed distribution we have outliners that will greatly affect our Mean but they will not affect the Median.  Median + Interquartile

In a Symmetrical distribution we would use the Mean. We can use either the Mean or Median however it is best to use the Mean as the T-test and further tests we will run all use the Mean. Mean + STD


Varriance -> We measure the spread around the Mean with the standard deviation, this is because the Mean as a data set is a reference mid-point and then we can calculate how much each data value deviates from the mean. How variable the data is around the mean, we can calculate this by using the Variance. The variance is the average of the squared deviations from the mean. The Standard Deviation is the square root of the variance.

Std -> Standard Deviation is the average distance of data from the Mean. We divide by n-1 when we are using a sample set and not the full population n.


Measures of centre -> which point is the distribution centred
Measures of spread -> how are the scores in the distribution spread out

Mean -> What most people refer to as the "average". Mean is the balance point of a distribution the Median should also match the Mean at this point and there should be a roughly equal spread to the right and left of the peak (binomal peak will see balance in the dip between the two peaks). To look at spread of Mean we use Standard Deviation.
mean = sum of data values / total number of data values

Median lies at the mid-point of a distribution, the Mean is the balance point of the distribution. For Symmetrical distributions these will closely line up. For Skewed distributions outliners will skew the Mean and we need to look to the Median. A characteristic of a skewed distribution can be seen by a noted difference in the Median and Mean. Because the Median utilises the interquartile range and does not include outliners it is said to be a resistant statistic. For this reason the Median can be used as as measure of typicality when the distribution is known to be highly skewed or contain outliners.

Median ->  50% of values are lower than or equal to < midpoint > 50% values higher than or equal to
For odd # of values the Median will be one of the data values
For even # of values the Median may not coincide with an actual data point
The Median divides a distribution in half.
As a measure of spread around the Median we use the interquartile range Q1 and Q3 because the Median is Q2.

Range -> simplest measure of distribution spread -> difference between largest and smallest values.
Because the range depends on two extreme values (largest, smallest) it is often not an informative measure of spread. The range does provide us with an indication of the absolute spread of the distribution.

Interquartile range ->  IQR = Q3 - Q1
The interquartile range is defined to be the spread of the middle 50% of data values (observations)
Q1  -> midpoint of the lower half of data values
Q2
Q3  -> midpoint of the upper half of data values

Since upper 25% and lower 25% of observations are discarded, no outliners are considered to be included which makes it a more meaningful measure of spread then the range.
A histogram can display the quartiles visually:
Q1 = bottom 25% of histogram is first quartile
Q2 = bottom 50% of histogram is second quartile  = Median
Q3 = top 25% of histogram is third quartile
IQR = marks off the middle 50% of data values and covers half the area of the histogram

Knowing the median and quartiles of a distribution means we know quite alot about the centre of the data set. The smallest and largest values of a dataset can give us a picture about the tails.

Five Number summary ->  Minimum Value, Q1, M, Q3, Maximum Value

Five Number Summary is the graphical representation of the five number summary. It is a compact way of displaying the location, spread and general shape of a distribution. It is also useful for comparing distributions of various related subgroups.

Box Plot -> box represents 50% of values, vertical line within the box represents the Median, lines (whiskers) are extended out from the lower and upper ends of the box to the smallest and largest values of the dataset respectively.  


Distributions

A Symmetric -> equally spread out around the median, Strong tendency for data values to cluster around the centre rather then the extremes -> on a boxplot the median is the middle of the box and the whiskers are approximatley equal in length.
Positively-skewed -> characterised by a cluster of data values at the left hand end of the distribution with a gradual tailing off to the right. -> boxplot has a median off-centre to the left. The left-hand whisker will be short while the right hand whisker will be long (reflecting the gradual tailing off to the right)
Negativley-skewed -> characterised by a clustering of data values to the right hand side of the distribution, with a gradual tailing off to the left -> boxplot Median off centre and to the right. Right hand whisker will be short while the left hand whisker will be long, reflecting the gradual tailing off of data values to the left.

Calculating Outliners
Outliner -> point which is more than  1.5IQR away from the 50% interquartile spread -> classified as possible outliner until confirmed with more data points -> 1.5 Box lengths from the edge of the box -> more then 3IQRs above Q3 or below Q1

Note: when there are outliners in a boxplot the higheest and lowest value will be shown not including the outliners

the IQR is 14 ->    1.5 x IQR = 1.5 x 14 = 21
                    Q1 - 1.5 x IQR = 61 - 21 = 40
                    Q3 - 1.5 x IQR = 75 - 21 = 96

Q1 = Lower Limit = 40
Q3 = Upper Limit = 96

IF your going to use a boxplot use Toukeys method for Q1 and Q3 as it will match the boxplot



Topic 4
