SPSS Work

Topic1 + Topic2 - Section One of Workbook

Why do we bother with levels of measurement?

The values of variables are interpreted as numbers, however these numbers represent different types of data that hold different meanings. Levels of Measurement gives us away to  distinguish between the various uses of Data. Each level of measurement gives rise to numbers which carry different amounts of information.


**How do we display categorical data?**

* frequency table
* bar graph
* pie chart

**How  do we summarize categorical data?**
Answers in exercise report .html and .docx files

**SPSS Specific Notes**

Variables:

Consider data looking at life expectancy in Australia. There are four variables Country, Sex, Year, Life Expectancy. The variable life expectancy is displayed in year sets these are the cases.

Values

SPSS prefers numerical values, when we are presented with categorical data such as Gender: Male, Female, Other we take these values and present them as numbers. Male: 1, Female 2, Other 3


**Levels of Measurement**

When we represent values we depict category's/names as numbers. Due to the variety of the data that can be represented in values we need a way to distinguish between the types of data represented and what they might mean in the context of the data analysis. In order to do this we have levels of measurement:

**Nominal Scale**

Lowest level of measurement. Nominal Scale numbers are used simply to "nominate" or "name". We do not perform calculations on Nominal Scale numbers as their values are meaningless and are for naming purposes only. It is important to note that Nominal Scale numbers are not ordered and therefore the order that we apply does not matter. Nominal Scale variables that only have two categories levels are refereed to as Binary or Dichotomous variables since the numbers are either 0 or 1, these can be seen as True or False answers.

Unknown Order of Values / Frequency / Mode


Nominal Scale
Type of Music: 0 Country, 1 Pop, 2 Reggae, 3 Hard Rock, 4 Techo

Binary/Dichotomous Scale
Completed a Bachelors Degree: 0 Yes, 1 No


**Ordinal Scale**

In ordinal scale numbers act as both labels and order, this means that unlike Nominal Scale the order of Ordinal Scale matters and changing this will affect the outcome and cause an incorrect and misleading outcome. Ordinal Scale numbers are numbers that fit into an ordered scaling set, this means that they can start at any number but that the numbers will scale up or down in their perceived order.

Known Order of Values / Frequency / Mode / Median

Ordinal Scale
Academic Markings: 0 HD, 1 D, 2 C, 3 P, 4 F

**Interval Scale**

Interval Scale numbers have all the properties of Ordinal Scale numbers but it also has the additional properties that the intervals between numbers are equal. In an Interval Scale the number 0 does not mean that the variable is absent, this means that 0 holds its own value. This is how we can see Ordinal Scale properties be seen in Interval Scale numbers. Because we do not have a true zero we can not do calculate the difference between values and add and subtract however we can not make ratio statements as we can not multiply and divide. Interval Scales are heavily used in Social Science (Psychology) where we have to measure abstract concepts. This is where a scale is applied to the questions asked which is an Ordinal Scale Number and then the total score of the questions is considered to be measured as an Interval Scale Number.

Known Order of Values / Frequency / Mode / Median / Mean / Difference between values / Add and Subtract Values

Interval Scale
Temperature: -5, -4, -3, -2, -1, 0, 1, 2, 3, 4
How do you Feel today: 0 Very Sad, 1 Sad, 2 Neither Sad nor Happy, 3 Happy, 4 Very Happy



**Ratio Scale**

The highest level of measurement. Ratio Scale has all the properties of Interval Scale however there is a true zero present in a Ratio Scale which means absence of measurement for that value. As Ratio Scale numbers are true representations we can use multiply and divide.

Known Order of Values / Frequency / Mode / Median / Mean / Difference between values / Add and Subtract Values / true zero / Multiply and Divide

Ratio Scale

Weight in KG: 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10


**Categorical and Metric Data**

The highest level of difference we can apply to values is Categorical or Metric. When we are assessing our variables which area they fall into will determine which methods of statistical analysis are applied.

Categorical - Nominal or Ordinal Data - The data falls into categories which are captured by words
Metric - Interval or Scale Data - The data can be labeled to both measure and order

Metric Variable SPSS Analyze -> Descriptive Statistics -> Explore

Histograms are good for showing interval data for large datasets they can be useful

Features of Histograms

Shape

How is the data distributed
Is it peaked or flat?
Is it single peaked? does it trail off evenly on either side of the peak
Symmetric distribution can be seen in a single peak with even tail or within a double peak we evaluate the dip inbetween the two peaks. Double peak distributions are said to be bimodal.
In a histogram the peak can be used to locate the mode of the frequency.
If the histrogram tails off primarily in one direction then it is said to be skewed;
Positive > Skewed Right
Negative > Skewed Left

Outliners

Outliners can be easily seen by unexpected peaks in distribution [ data values that appear to be atypically high or low ]
=======
Topic Two
Identify distributions of a metric variable given a histogram or stemplot as one of the following:

* approximately symmetric
* positivley skewed
* negativley skewed distributions
* Bimodal

Read Stem and Leaf plots: identify minimum, maximum, proportion of values below or above given values.

Centre and Spread
Single Peaked :
  middle of distribution -> characteristic of symmetric distribution
  one side - > characteristic of skewed distribution
  is the peak narrow -> most data values are tightly clustered in a small region
  is the peak broad -> data values are widely spread out

  example --->> There is a broad peak indicating that although the distribution has a clear centre (somewhere around the peak region) the data values are not tightly clustered around this peak.
  --->> There is a narrow peak indicating that the data values are tightly clustered around the centre of the distribution.


Stem and Leaf Plot is alternative to the historgram - useful for displaying small sets of data (15 to 200) (histogram is best for larger sets of data)
Stemplot like a histogram provides information on shape, outliers and centre and spread of the distribution. A Stemplot is different from most other graphs as a Stemplot retains all the values from the dataset. They are a great way to have a quick look at all the data values.

The data value is seperated into two parts;
STEM [the leading digits] | LEAF trailing digits  
